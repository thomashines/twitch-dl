#!flask run --reload -h 0.0.0.0


import contextlib
import datetime
import flask
import os
import re
import requests
import sqlite3
import sys
import urllib.parse


from shared import open_db, subscription_fields, video_fields, videos_refresh


app = flask.Flask(__name__)


# northernlion 14371185
# set -x FLASK_APP twitch-dl.py
# set -x FLASK_ENV development
# flask run --reload -h 0.0.0.0


def generic_get(table, fields, order):
    id = flask.request.values.get("id")
    if id is not None and len(id) > 0:
        with open_db() as cur:
            cur.execute(("select {fields_joined} " +
                "from {table} " +
                "where id=?;").format(
                    fields_joined=", ".join(fields),
                    table=table), (id,))
            row = cur.fetchone()
            if row is None:
                return flask.jsonify({"error": "id not found"}), 404
            else:
                return flask.jsonify(dict(zip(fields, row))), 200
    else:
        rows = []
        with open_db() as cur:
            cur.execute(("select {fields_joined} " +
                "from {table} " +
                "order by {order};").format(
                    fields_joined=", ".join(fields),
                    order=order,
                    table=table))
            rows = tuple(dict(zip(fields, row)) for row in cur.fetchall())
        return flask.jsonify({table: rows}), 200


def generic_put(table, fields):
    data = dict((field, flask.request.values.get(field))
        for field in fields if field in flask.request.values)
    data_pairs = tuple(data.items())
    data_fields = tuple(pair[0] for pair in data_pairs)
    data_values = tuple(pair[1] for pair in data_pairs)
    id = data.get("id")
    if id is not None and len(id) > 0:
        with open_db() as cur:
            cur.execute(("select {fields_joined} " +
                "from {table} " +
                "where id=?;").format(
                    fields_joined=", ".join(fields),
                    table=table), (id,))
            existing = cur.fetchone()
            status = None
            if existing is None:
                status = 201
                cur.execute(("insert into {table} ({data_fields_joined}) " +
                    "values ({question_marks});").format(
                        data_fields_joined=", ".join(data_fields),
                        question_marks=", ".join("?" for value in data_values),
                        table=table), data_values)
            else:
                status = 200
                cur.execute(("update {table} " +
                    "set {data_joined} " +
                    "where id=?;").format(
                        data_joined=", ".join("{}=?".format(field)
                            for field in data_fields),
                        table=table), data_values + (id,))
            cur.execute(("select {fields_joined} " +
                "from {table} " +
                "where id=?;").format(
                    fields_joined=", ".join(fields),
                    table=table), (id,))
            new = cur.fetchone()
            return flask.jsonify(dict(zip(fields, new))), status
    return flask.jsonify({"error": "invalid input"}), 400


def generic_delete(table, fields):
    id = flask.request.values.get("id")
    if id is not None and len(id) > 0:
        with open_db() as cur:
            cur.execute(("select {fields_joined} " +
                "from {table} " +
                "where id=?;").format(
                    fields_joined=", ".join(fields),
                    table=table), id)
            existing = cur.fetchone()
            status = None
            if existing is None:
                return flask.jsonify({"error": "id not found"}), 404
            else:
                cur.execute(("delete from {table} " +
                    "where id=?;").format(
                        table=table), id)
                return flask.jsonify(dict(zip(fields, existing))), 200
    return flask.jsonify({"error": "invalid input"}), 400


@app.route("/subscriptions", methods=["GET"])
def subscriptions_get():
    return generic_get("subscriptions", subscription_fields, "name asc")


@app.route("/subscriptions", methods=["POST", "PUT"])
def subscriptions_put():
    return generic_put("subscriptions", subscription_fields)


@app.route("/subscriptions", methods=["DELETE"])
def subscriptions_delete():
    return generic_delete("subscriptions", subscription_fields)


@app.route("/videos", methods=["GET"])
def videos_get():
    update = flask.request.values.get("update")
    if update == "true":
        videos_refresh()
    return generic_get("videos", video_fields, "uploaded desc")


@app.route("/videos", methods=["POST", "PUT"])
def videos_put():
    return generic_put("videos", video_fields)


@app.route("/")
@app.route("/static/<path:path>")
def static_handler(path=None):
    if path is None or len(path) == 0:
        path = "index.html"
    return flask.send_from_directory("static", path)


if __name__ == "__main__":
    print("started")
    while True:
        print("work")
        time.sleep(background_rate)

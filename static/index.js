"use strict";


var app = new Vue({
  el: "#app",
  data: {
    message: "loading",
    subscriptions: [],
    videos: [],
  },
  mounted: function () {
    this.refresh(false);
  },
  methods: {

    refresh: function (update) {
      let self = this;
      self.message = "refreshing";
      let data = {};
      if (update) {
        data.update = true;
      }
      ["subscriptions", "videos"].forEach((table) => {
        $.ajax({
          method: "GET",
          url: table,
          data: data,
        }).done((body) => {
          self.message = "got " + table;
          self[table] = body[table];
        }).fail(() => {
          self.message = "failed to get " + table;
        });
      });
    },

    subscriptions_add: function () {
      let self = this;
      self.subscriptions.push({
        "id": "",
        "name": "",
      });
    },

    subscriptions_save: function (subscription) {
      let self = this;
      self.message = "saving";
      $.ajax({
        method: "PUT",
        url: "subscriptions",
        data: subscription
      }).done((body) => {
        self.message = "ready";
      }).fail(() => {
        self.message = "failed";
      });
    },

    subscriptions_delete: function (subscription) {
      let self = this;
      self.message = "deleting";
      $.ajax({
        method: "DELETE",
        url: "subscriptions",
        data: subscription
      }).done((body) => {
        self.message = "ready";
      }).fail(() => {
        self.message = "failed";
      });
    },

    videos_save: function (video, field) {
      let self = this;
      self.message = "saving";
      let data = {id: video.id};
      if (field === "download") {
        video.download = video.download ? 1 : 0;
        data.download = video.download;
      } else if (field === "watched") {
        video.watched = video.watched ? 1 : 0;
        data.watched = video.watched;
      } else {
        data = video;
      }
      $.ajax({
        method: "PUT",
        url: "videos",
        data: data
      }).done((body) => {
        self.message = "ready";
      }).fail(() => {
        self.message = "failed";
      });
    },

  },
});

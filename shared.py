#!/usr/bin/env python


import contextlib
import datetime
import flask
import os
import re
import requests
import sqlite3
import sys
import urllib.parse


subscription_fields = ("id", "name")

video_fields = ("id", "download", "length", "path", "progress", "subscription",
    "thumbnail_path", "thumbnail_url", "title", "uploaded", "url", "watched")


@contextlib.contextmanager
def open_db():
    with sqlite3.connect("twitch-dl.db", detect_types=sqlite3.PARSE_DECLTYPES) as conn:
        cur = conn.cursor()
        cur.execute("create table if not exists subscriptions (" +
            "id text not null primary key, " +
            "name text not null);")
        cur.execute("create table if not exists videos (" +
            "id text not null primary key, " +
            "download bool, " +
            "length real not null, " +
            "path text, " +
            "progress real not null, " +
            "subscription id not null, " +
            "thumbnail_path text, " +
            "thumbnail_url text, " +
            "title text not null, " +
            "uploaded timestamp, " +
            "url text not null, " +
            "watched bool);")
        conn.commit()
        yield cur
        conn.commit()


def twitch_request(method, url, params={}, headers={}):
    url = "https://api.twitch.tv/helix/" + url
    headers = dict({
        "Accept": "application/json",
        "Client-ID": "ewkdy9x1p4rhh4jehx7ugdh2gjzkcn",
    }, **headers)
    param_delim = "&" if "?" in url else "?"
    cursor = None
    while True:
        this_url = url
        if cursor is not None:
            params["after"] = cursor
        params_encoded = urllib.parse.urlencode(params)
        if len(params_encoded) > 0:
            this_url += "?" + params_encoded
        response = requests.request(method, this_url, headers=headers).json()
        cursor = response.get("pagination", {}).get("cursor")
        data = response.get("data", [])
        if len(data) == 0:
            break
        else:
            for datum in data:
                yield datum


periods = [
    {
        "pattern": re.compile("(\d+) ?(?:seconds|second|secs|sec|s)"),
        "seconds": 1.0,
    },
    {
        "pattern": re.compile("(\d+) ?(?:minutes|minute|mins|min|m)"),
        "seconds": 60.0,
    },
    {
        "pattern": re.compile("(\d+) ?(?:hours|hour|hrs|hr|h)"),
        "seconds": 60.0 * 60.0,
    },
    {
        "pattern": re.compile("(\d+) ?(?:days|day|d)"),
        "seconds": 24.0 * 60.0 * 60.0,
    },
]

def parse_duration(duration):
    duration = duration.lower()
    seconds = 0.0
    for period in periods:
        period_seconds = period["seconds"]
        for match in period["pattern"].finditer(duration):
            seconds += period_seconds * int(match.group(1))
    return seconds


def videos_refresh():
    field_map = {
        "id": lambda d: d.get("id"),
        "length": lambda d: parse_duration(d.get("duration", "")),
        "thumbnail_url": lambda d: d.get("thumbnail_url"),
        "title": lambda d: d.get("title"),
        "uploaded": lambda d: datetime.datetime.strptime(
            d.get("created_at", ""), "%Y-%m-%dT%H:%M:%SZ"),
        "url": lambda d: d.get("url"),
    }
    defaults = {
        "download": True,
        "progress": 0,
        "watched": False,
    }
    to_insert = []
    with open_db() as cur:
        subscriptions = cur.execute("select id " +
            "from subscriptions;")
        for subscription in subscriptions:
            defaults["subscription"] = subscription[0]
            videos = twitch_request("GET", "videos", {
                "first": 100,
                "type": "archive",
                "user_id": subscription[0],
            })
            for video in videos:
                parsed = dict(defaults)
                for field, get in field_map.items():
                    parsed[field] = get(video)
                cur.execute("select id " +
                    "from videos " +
                    "where id=?;", (parsed["id"],))
                existing = cur.fetchone()
                if existing is None and len(parsed.get("thumbnail_url", "")) > 0:
                    to_insert.append(tuple(parsed.get(field)
                        for field in video_fields))

        if len(to_insert) > 0:
            cur.executemany(("insert into videos ({fields_joined}) " +
                "values ({question_marks});").format(
                    fields_joined=", ".join(video_fields),
                    question_marks=", ".join("?" for field in video_fields)),
                to_insert)

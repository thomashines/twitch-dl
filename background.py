#!/usr/bin/env python


import time
import urllib.request
import youtube_dl


from shared import open_db, videos_refresh


thumbnail_width = 320
thumbnail_height = 180

background_rate = 60 * 60


def get_thumbnails():
    with open_db() as cur:
        videos = cur.execute("select id, thumbnail_url " +
            "from videos " +
            "where thumbnail_path is null;")
        for video in videos.fetchall():
            id = video[0]
            thumbnail_url = video[1]
            thumbnail_url = thumbnail_url.replace("%{width}",
                str(thumbnail_width))
            thumbnail_url = thumbnail_url.replace("%{height}",
                str(thumbnail_height))
            thumbnail_path = "static/thumbnails/{}-{}".format(id,
                thumbnail_url.split("/")[-1])
            with open(thumbnail_path, "wb") as f:
                f.write(urllib.request.urlopen(thumbnail_url).read())
            cur.execute("update videos " +
                "set thumbnail_path=? " +
                "where id=?;", (thumbnail_path, id))


def progress_hook(id, status, last_status):
    if "filename" not in last_status:
        with open_db() as cur:
            cur.execute("update videos " +
            "set path = ? " +
            "where id = ?;", (status["filename"], id))
    last_status["filename"] = status["filename"]
    if "_percent_str" in status:
        percent = float(status["_percent_str"].replace("%", ""))
        with open_db() as cur:
            cur.execute("update videos " +
                "set progress = ? " +
                "where id = ?;", (percent, id))


def download_videos():
    options = {
        "format": "bestvideo[height<=480]+bestaudio/best[height<=480]",
        "outtmpl": "static/videos/%(id)s.%(ext)s",
        "progress_hooks": (progress_hook,),
        "quiet": True,
        "ratelimit": 256 * (2 ** 10),
    }
    videos = None
    with open_db() as cur:
        videos = cur.execute("select id, url " +
            "from videos " +
            "where progress < 100.0 " +
            "and download = 1 " +
            "order by uploaded desc;")
        videos = videos.fetchall()
    for video in videos:
        id = video[0]
        url = video[1]
        last_status = {}
        options["progress_hooks"] = (
            lambda status: progress_hook(id, status, last_status),
        )
        with youtube_dl.YoutubeDL(options) as ytdl:
            ytdl.download((url,))


if __name__ == "__main__":
    while True:
        videos_refresh()
        get_thumbnails()
        download_videos()
        time.sleep(background_rate)
